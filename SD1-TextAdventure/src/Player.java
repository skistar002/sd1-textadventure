
public class Player {
	String name, weapon;
	String[] inventory;
	int score, location;
	public Player (String name, int location, String[] inventory, int score, String weapon) {
		this.name = name;
		this.location = location;
		this.inventory = inventory;
		this.score = 5; // starts at Desert
		this.weapon = weapon;
	}

	@Override
	public String toString() {
		return this.name + " " + this.location + " " + this.score + this.inventory;
	}
}

/**
 * Tyler Galske
 * Project 2
 * CMPT 220L 114
 * 2/25/16
 * 		Caves(0)	Jungle(1)		Village(2)
 * 		Beach(3)	Desert(4)		River(5)
 * 		Plains(6)	Mountain(7)		Cliffs(8)
 */

import java.util.Scanner;

/**
 * @author Tyler Galske
 *
 */
public class TextAdventure {
	
	// constants
	final static int NORTH = 0;
	final static int EAST = 1;
	final static int SOUTH = 2;
	final static int WEST = 3;
	
	static Locale[] locations = {
		 new Locale("Caves", "It is cold and damp.", "Flashlight", false, false)
		,new Locale("Jungle", "It is hot and humid.", "Machete", false, false)
		,new Locale("Village", "There are huts in a circle.", "Batteries", false, false)
		,new Locale("Beach", "There is warm, golden sand.", "Mirror", false, false)
		,new Locale("Desert", "Sun is beating down on you.", "Shotgun", false, false)
		,new Locale("River", "Fish are jumping in the rapids.", "Canteen", false, false)
		,new Locale("Plains", "Flat grass stretching for miles.", "Map", false, false)
		,new Locale("Mountain", "A snowy and windy peak.", "Shotgun Shells", false, false)
		,new Locale("Cliffs", "Steep and dangerous rocks.", "Radio", false, false)
	};
	
	// navigation matrix
	static int[][] map = {
		 {-1, 1, 3, -1}
		,{-1, 2, 4, 0}
		,{-1, -1, 5, 1}
		,{0, 4, 6, -1}
		,{1, 5, 7, 3}
		,{2, -1, 8, 4}
		,{3, 7, -1, -1}
		,{4, 8, -1, 6}
		,{5, -1, -1, 7}
	};
	
	public static void main(String[] args) {
		
		//declarations
		Player User = new Player(null, 4, null, 5, null); // instance of Player
		User.inventory = new String[9];
		
		Scanner keyboard = new Scanner(System.in);
		String userInput; // used for entering commands
						
		// Beginning of game
		System.out.println("Island Text Adventure");
		System.out.print("Howdy! What is your name? ");
		User.name = keyboard.nextLine();
		System.out.print("Howdy, " + User.name + "! What is your weapon of choice? ");
		User.weapon = keyboard.nextLine();
		System.out.println("Ok, " + User.name + ". You are at the " 
				+ locations[User.location].name 
				+ ". " + locations[User.location].description);
		
		// User input for commands, directions
		while (true) {
			System.out.print("\nEnter a command: ");
			userInput = keyboard.nextLine().toLowerCase().trim();
			
			if (userInput.equals("n")) {
				move(NORTH, User);
				printLoc(User);
			} else if (userInput.equals("e")) {
				move(EAST, User);
				printLoc(User);
			} else if (userInput.equals("s")) {
				move(SOUTH, User);
				printLoc(User);
			} else if (userInput.equals("w")) {
				move(WEST, User);
				printLoc(User);
			} else if (userInput.equals("weapon")) {
				System.out.print("Your weapon: " + User.weapon + "\n");
			} else if (userInput.equals("q")) {
				break; // gets out of the game loop
			} else if (userInput.equals("h")) {
				displayHelp();
			} else if (userInput.equals("m")) {
				displayMap(User);
			} else if (userInput.equals("t")) {
				takeItem(User); // adds item to User inventory
			} else {
				System.out.println("Invalid command!");
				continue;
			}						
		} // end of loop
		
		displayCredits();
		keyboard.close(); // cleans up resources
		
	} // end of main method

	static void playerScore(Player User) {
		if (locations[User.location].visited == false) {
			locations[User.location].visited = true;
			User.score += 5;
			System.out.println("Score: " + User.score);
		}
	}
	
	static void takeItem(Player User) {
		if (locations[User.location].itemTaken == false) {
			System.out.println("You have taken the " 
					+ locations[User.location].item 
					+ ".");
			// adds item to User inventory
			User.inventory[User.location] = locations[User.location].item;
			locations[User.location].itemTaken = true;
		} else {
			System.out.println("You have already taken the " 
					+ locations[User.location].item + ".");
		}
	}
	static int from (Player User, int loc, int dir) {
		int locNum = User.location;
		return map[locNum][dir];
	}
	static void move (int dir, Player User) {
		int nextLocation = from(User, User.location, dir);
		if (nextLocation != -1) {
			User.location = nextLocation;
		} else {
			System.out.println("Try a different direction.");
		}
	}
	static void printLoc(Player User) {
		System.out.println(locations[User.location]);
		playerScore(User); // calls the point tracking method
	}
	static void displayHelp() {
		String help = "\nWelcome to Island Text Adventure. \n"
			+ "First, enter your name and weapon of choice. \n"
			+ "Next, use the commands N, E, S, W to move around the island. \n"
			+ "\"q\" quits the game.\n"
			+ "\"weapon\" shows your current weapon.";
		System.out.println(help);
	}
	static void displayCredits() {
		// Goodbye message
		System.out.println("Thank you for playing Island Text Adventure!");
		System.out.print("Tyler Galske -  \u00A9 2016 \n");
	}
	static void displayMap(Player User) {
		String map = "Caves    Jungle    Village\n" +
		 			 "Beach    Desert    River\n" + 
		 			 "Plains   Mountain  Cliffs\n";
		if ("Map".equals(User.inventory[6])) {
			System.out.println(map);
		} else {
			System.out.println("Find the map first!");
		}
	}
} // end of TextAdventure class
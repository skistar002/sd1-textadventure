
public class Locale {
	String name, description, item;
	Boolean itemTaken,visited;
	public Locale (String name, String description, String item, Boolean itemTaken, Boolean visited) {
			this.name = name;
			this.description = description;
			this.item = item;
			this.itemTaken = false;
			this.visited = false;
	}
	
	@Override
	public String toString() {
		return "Location: the " + this.name + ". " + this.description;
	}
}